package com.nexsoft.discite.Repository;

import com.nexsoft.discite.Entity.Soal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SoalRepository extends JpaRepository<Soal, Long> {
}
