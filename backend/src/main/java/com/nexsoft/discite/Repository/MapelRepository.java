package com.nexsoft.discite.Repository;

import com.nexsoft.discite.Entity.Mapel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MapelRepository extends JpaRepository<Mapel, Long> {
}
