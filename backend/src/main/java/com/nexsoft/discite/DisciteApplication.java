package com.nexsoft.discite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DisciteApplication {

    public static void main(String[] args) {
        SpringApplication.run(DisciteApplication.class, args);
    }

}
